import angular from 'angular'
import router from './router-config'

angular.module('app', []).config(router)
